// variable declartions


let openside = document.getElementById('side-menu');
let sidebar = document.getElementById("opensidebar");
let link = document.querySelector('.nav-link');
let image = document.querySelectorAll(".ui.small.images");
let closebar = document.getElementById('closeside');
let modal = document.querySelector('.ui.modal');
let realmodal = modal.querySelector('.ui.modal img');
let realheader = modal.querySelector('.description div');
let realpara = modal.querySelector('.description p');
let dimmer = document.querySelector('.ui.dimmer.modals.page.transition.hidden');
let header = document.querySelectorAll('.ui.center.aligned.container h4');
let icon = modal.firstElementChild;
let column = document.querySelectorAll('div.column.col');
for(let index = 0;index<column.length;index++){
    column[index].addEventListener('mouseover',()=>{
        let child = column[index].firstElementChild;
        child.style.transition = "0.5s";
        child.setAttribute('class', 'shadow');
    });
    
}
for(let i =0;i<column.length;i++){
    column[i].addEventListener('mouseout',()=>{
        let child = column[i].firstElementChild;
        child.style.transition = "0.2s";
        child.style.border = "none";
        child.removeAttribute('class', 'ui raised segment');
    });
}

sidebar.addEventListener('click', () => {
    openside.style.width = "19rem";
    sidebar.style.display = 'none';
});
closebar.addEventListener('click', () => {
    openside.style.width = "0px";
    sidebar.style.display = "block";
});

icon.addEventListener('click', () => {
    modal.setAttribute('class', 'ui modal scrolling transition hidden');
    dimmer.setAttribute('class', "ui dimmer modals page transition hidden");
});
for (let i = 0; i < image.length; i++) {
    let img = image[i].firstElementChild;
    image[i].addEventListener('click', () => {
        realheader.innerHTML = header[i].innerHTML;
        realmodal.setAttribute('src', img.getAttribute('src'));
        if (dimmer.className === "ui dimmer modals page transition hidden") {
            dimmer.setAttribute('class', "ui dimmer modals page transition visible active");
            dimmer.style = "display:flex!important";
        } else {
            dimmer.setAttribute('class', "ui dimmer modals page transition hidden");
        }
        if (modal.className === "ui modal scrolling transition hidden") {
            modal.setAttribute('class', "ui modal scrolling transition visible active");
            modal.style = "display:flex!important";
        } else {
            modal.setAttribute('class', 'ui modal scrolling transition hidden');
        }

    });
}
window.addEventListener('click', clickoutside);
function clickoutside(e){
   if(e.target ==dimmer){
    modal.className = "ui modal scrolling transition hidden";
    dimmer.className = "ui dimmer modals page transition hidden";
   }
}
// jquery code.

$(document).ready(function(){
    // All your normal JS code goes in here
    $(".rating").rating();
});